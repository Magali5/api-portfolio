<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\Project;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Service\FileService;
use Symfony\Component\HttpFoundation\Response;

//ici c'est la route principale
/**
 * @Route("/project", name="projet")
 */

class ProjectController extends Controller
{
    private $serializer;
    
    public function __construct(SerializerInterface $serializer) {
        $this->serializer = $serializer;
    }
    /**
     * @Route("/", methods={"GET"})
     */
    //ici cette route veut dire que tout ce qui est après n'est accessible car par la méthode get

    public function all(){
        $repo = $this->getDoctrine()->getRepository(Project::class);
        $projects = $repo->findAll();
        $json = $this->serializer->serialize($projects, "json");

        return JsonResponse::fromJsonString($json);
    }


/**
 * @Route("/", methods={"POST"})
 */
       public function add(Request $req, FileService $fileService) {
        //On récupère le fichier dans la request
        $image = $req->files->get("image");
        //On récupère l'url absolue de notre application
        $absoluteUrl = $req->getScheme() . '://' . $req->getHttpHost() . $req->getBasePath();
        //On utilise le fileService pour uploader l'image
        $imageUrl = $fileService->upload($image, $absoluteUrl);

        $project = new Project();
        $project->setName($req->get("name"));

        $project->setDescription($req->get("description"));
        $project->setLink($req->get("link"));
  
        $project->setImage($imageUrl);

        $manager = $this->getDoctrine()->getManager();
        $manager->persist($project);
        $manager->flush();

        $json = $this->serializer->serialize($project, "json");

        return JsonResponse::fromJsonString($json);


    }
        /**
 * @Route("/{project}", methods={"GET"})
 */
public function single(Project $project){
   $json = $this->serializer->serialize($project, "json");

    return JsonResponse::fromJsonString($json);
}

/**
 * @Route("/delete/{project}", methods={"DELETE"})
 */
public function remove(Project $project){
    $manager = $this->getDoctrine()->getManager();
    $manager->remove($project);
    $manager->flush();
    return new Response("ok", 204);
 }

 /**
 * @Route("/{project}", methods={"PUT"})
 */
public function update(Project $project, Request $request){
    $body = $request->getContent();
    $updated = $this->serializer->deserialize($body, Project::class, "json");

    $manager = $this->getDoctrine()->getManager();

    $project->setName($updated->getName());
    $project->setSurname($updated->getSurname());
    $project->setLevel($updated->getLevel());
    $project->setTech($updated->getTech());

    $manager->flush();
    $data = $this->serializer->normalize($project, null,['attributes'=>['id','name','description','link','image']]);

    return new Response($this->serializer->serialize($data,'json'));
 }
}


